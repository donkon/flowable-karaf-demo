# Introduction

This is the most minimal setup to get flowable running with Apache Karaf.

* The *karaf-feature* module contains a feature.xml file for installing all required bundles, a H2 datasource and a demo application using the Flowable API. To avoid classpath issues, the required flowable libraries will be embedded in the process-engine jar. Therefor, the *flowable* feature is *not* used.
* The *karaf-commands* module provides *flowable* commands for deploying BPMNs and starting a workflow instances
* The *process-engine* module contains the flowable-engine setup and a custom  BlueprintContextELResolver, so that beans in multiple Blueprint Containers (filtered by bundle symbolic name ending with 'tasks') can be resolved from ServiceTask expressions
* The *demo-tasks* module contains a simple POJO (registered in blueprint), which will by be consumed from a ServiceTask of the *demo-1* workflow
* The *sample-workflow* module contains a simple BPMN workflow, which can be deployed and started by flowable-karaf commands (see example below)

# Build

```
mvn clean install
```

# Karaf

## Get latest 4.2.6 distribution

http://karaf.apache.org/download.html

## Run

```
bin/karaf
```

## Install flowable features and H2 Datasource

```
feature:repo-add mvn:org.flowable.osgi.karaf.demo/karaf-feature/1.0.0-SNAPSHOT/xml/features
feature:install flowable-demo
```

## Check to see all bundles and Datasource active

```
bundle:list
service:list javax.sql.DataSource
```

## Install BPMN from sample-workflow bundle

```
flowable:deploy-bpmn sample-workflow
```

## Start a process instance

```
flowable:init-workflow demo-1 input:hello-world!
```
