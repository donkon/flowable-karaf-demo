package org.flowable.osgi.karaf.demo.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringOperations {
	
	private static final Logger LOGGER =  LoggerFactory.getLogger(StringOperations.class);
	
	public int getSize(String item) {
		LOGGER.info("Berechne String Länge von " + item);
		if (item != null) {
			return item.length();
		}
		return 0;
	}

}
