
package org.flowable.osgi.karaf.demo.commands;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.karaf.shell.api.action.Action;
import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.lifecycle.Reference;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.runtime.ProcessInstance;

@Command(scope = "flowable", name = "init-workflow", description = "Starts a bpmn workflow")
@Service
public class InitWorkflowCommand implements Action {

    @Argument(index = 0, name = "processDefinitionKey", description = "The process definition key", required = true, multiValued = false)
    private String processDefinitionKey;
    
    @Argument(index = 1, name = "params", description = "Colon-sperated process variables, e.g param1:value1 param2:value2", required = false, multiValued = true)
    private Collection<String> params;
    
    @Reference
    private RuntimeService runtimeService;
    
    @Override
    public Object execute() throws Exception {
    	ProcessInstance processInstance = this.runtimeService.startProcessInstanceByKey(processDefinitionKey, addParams());
    	return "Process-Instance started: " + processInstance.getId();
    }

	private Map<String, Object> addParams() {
		Map<String, Object> paramMap = new HashMap<>();
		for (String param : params) {
			String[] split = param.split(":");
			if (split.length == 2) {
				paramMap.put(split[0], split[1]);
			} else {
				throw new IllegalArgumentException("Params must be seperated by colons, e.g. param1:value1");
			}
		}
		return paramMap;
	}
    

}
