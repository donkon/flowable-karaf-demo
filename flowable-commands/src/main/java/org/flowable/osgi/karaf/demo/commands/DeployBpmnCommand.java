
package org.flowable.osgi.karaf.demo.commands;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Enumeration;

import org.apache.karaf.shell.api.action.Action;
import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.lifecycle.Reference;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.DeploymentBuilder;
import org.flowable.engine.repository.ProcessDefinition;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

@Command(scope = "flowable", name = "deploy-bpmn", description = "Installs all bpmn process definitions within a bundle found under /bpmn")
@Service
public class DeployBpmnCommand implements Action {

    @Argument(name = "bundleName", description = "Symbolic name of the bundle", required = true, multiValued = false)
    private String bundleName;
    
    @Reference
    private RepositoryService repositoryService;
    
    @Reference
    private BundleContext bundleContext;

    @Override
    public Object execute() throws Exception {
    	Bundle[] bundles = this.bundleContext.getBundles();
		for (Bundle currentBundle : bundles) {
			if (currentBundle.getSymbolicName() != null && currentBundle.getSymbolicName().equals(bundleName)) {
				return installDefinition(currentBundle);
			}
		}
		return "Bundle '" + this.bundleName + "' not found";
    }
    
    private String installDefinition(Bundle bundle) throws IOException {
		Enumeration<URL> entries = bundle.findEntries("/", "*.bpmn", true);
		if (entries != null) {
			DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();
			while (entries.hasMoreElements()) {
				URL element = entries.nextElement();
				deploymentBuilder.addInputStream(element.getFile() + ".bpmn", element.openStream());
				deploymentBuilder.name(bundle.getLocation() + " - " + new Date(bundle.getLastModified()));
				String deploymentId = deploymentBuilder.deploy().getId();
				ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deploymentId).singleResult();
				if (processDefinition != null) {
					return "Process '" + element.getFile() + "' deployed successfully: DeploymentId '" + deploymentId + "', ProcessDefinitionId: '" + processDefinition.getId() + "'";
				} else {
					throw new RuntimeException("Process could not be deployed: '" + deploymentId + "' not found");
				}
			}
		} else {
			return "No BPMN-Process-Definition found in bundle " + bundle;
		}
		return null;
	}
}
