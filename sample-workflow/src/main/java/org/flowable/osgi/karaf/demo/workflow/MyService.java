package org.flowable.osgi.karaf.demo.workflow;

public interface MyService {
	
	public String doSomething(String id);
	
}
