package org.flowable.osgi.karaf.demo.workflow;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * @author bkonrad
 *
 */
public class InitWorkflow implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) {
		String useScript = (String) execution.getVariable("useScript");
		if ("true".equals(useScript)) {
			execution.setVariable("asScript", true);
			return;
		}
		execution.setVariable("asScript", false);
	}

}
