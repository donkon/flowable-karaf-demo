package org.flowable.osgi.karaf.demo.blueprint.resolver;

import java.util.HashMap;
import java.util.Map;

import org.flowable.common.engine.impl.AbstractEngineConfiguration;
import org.flowable.common.engine.impl.javax.el.ELContext;
import org.flowable.common.engine.impl.scripting.Resolver;
import org.flowable.common.engine.impl.scripting.ResolverFactory;
import org.flowable.osgi.blueprint.BlueprintContextELResolver;
import org.flowable.variable.api.delegate.VariableScope;
import org.osgi.framework.ServiceReference;
import org.osgi.service.blueprint.container.BlueprintContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Flowable EL Resolving for beans registered in Blueprint Container.
 * 
 * Registers only blueprint containers within a bundle having a symbolic name ending with 'tasks'.
 * 
 * @author bjoernkonrad@gmail.com
 *
 */
public class BlueprintFlowableELResolver extends BlueprintContextELResolver implements Resolver, ResolverFactory {
	
	private static final Logger LOGGER =  LoggerFactory.getLogger(BlueprintFlowableELResolver.class);
	
	private String key;

	private Map<String, BlueprintContainer> blueprintContainers = new HashMap<>();

	@SuppressWarnings("rawtypes")
	public void bind(ServiceReference reference) {
		if (reference.getBundle().getSymbolicName().endsWith("tasks")) {
			String symbolicname = (String) reference.getProperty("osgi.blueprint.container.symbolicname");
			if (blueprintContainers.containsKey(symbolicname)) {
				LOGGER.warn("Blueprint-Container '" + symbolicname + "' already exists");
			} else {
				key = symbolicname;
			}
		}
	}

	public void bind(BlueprintContainer service) {
		if (key != null) {
			blueprintContainers.put(key, service);
			LOGGER.info("Blueprint-Container '" + key + "' available for Flowable EL-Resolving");
			key = null;
		}
	}

	@SuppressWarnings("rawtypes")
	public void unbind(ServiceReference reference) {
		if (reference.getBundle().getSymbolicName().endsWith("tasks")) {
			String symbolicname = (String) reference.getProperty("osgi.blueprint.container.symbolicname");
			this.blueprintContainers.remove(symbolicname);
			LOGGER.info("Blueprint-Container '" + symbolicname + "' unavailable for Flowable EL-Resolving");
		}
	}

	@Override
	public Object getValue(ELContext context, Object base, Object property) {
		if (base == null) {
			String key = (String) property;
			for (BlueprintContainer blueprintContainer : blueprintContainers.values()) {
				for (String componentId : blueprintContainer.getComponentIds()) {
					if (componentId.equals(key)) {
						context.setPropertyResolved(true);
						return blueprintContainer.getComponentInstance(key);
					}
				}
			}
		}
		return null;
	}

	@Override
	public boolean containsKey(Object key) {
		for (BlueprintContainer blueprintContainer : blueprintContainers.values()) {
			for (String componentId : blueprintContainer.getComponentIds()) {
				if (componentId.equals(key)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public Object get(Object key) {
		for (BlueprintContainer blueprintContainer : blueprintContainers.values()) {
			for (String componentId : blueprintContainer.getComponentIds()) {
				if (componentId.equals(key)) {
					return blueprintContainer.getComponentInstance((String) key);
				}
			}
		}
		return null;
	}

	@Override
	public Resolver createResolver(AbstractEngineConfiguration arg0, VariableScope arg1) {
		return this;
	}

}
