package org.flowable.osgi.karaf.demo.blueprint.resolver;

import org.flowable.common.engine.impl.interceptor.AbstractCommandInterceptor;
import org.flowable.common.engine.impl.interceptor.Command;
import org.flowable.common.engine.impl.interceptor.CommandConfig;

public class SetContextClassLoaderInterceptor extends AbstractCommandInterceptor {

	@Override
	public <T> T execute(CommandConfig config, Command<T> command) {
		Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
		return next.execute(config, command);
	}

}
